# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 09:15:15 2021

@author: prern
"""

#For img 004 1.5, 22, 0.15
import cv2
image = cv2.imread("C:\\Users\prern\Desktop\img_004.png") # reading the image
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # convert2grayscale
(thresh, binary) = cv2.threshold(gray,0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU) # convert2binary
cv2.imshow('binary', binary)
cv2.imwrite('binary.png', binary)
(contours,hierarchy) = cv2.findContours(~binary,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
#contours= cv2.findContours(~binary,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 

# find contours
for contour in contours:
    """
    draw a rectangle around those contours on main image
    """
    [x,y,w,h] = cv2.boundingRect(contour)
    cv2.rectangle(image, (x,y), (x+w,y+h),(0,255,0), 1)
cv2.imshow('contour', image)
cv2.imwrite('contours.png', image)
cv2.waitKey(0)
cv2.destroyAllWindows()

import numpy as np
mask = np.ones(image.shape[:2], dtype="uint8") * 255 # create blank image of same dimension of the original image
contours,hierarchy = cv2.findContours(~binary,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
heights = [cv2.boundingRect(contour)[3] for contour in contours] # collecting heights of each contour
avgheight = sum(heights)/len(heights) # average height
# finding the larger contours
# Applying Height heuristic
for c in contours:
    [x,y,w,h] = cv2.boundingRect(c)
    if h > 1*avgheight:
        cv2.drawContours(mask, [c], -1, 0, -1)
cv2.imshow('filter', mask)
cv2.imwrite('filter.png', mask)

#RLSA

from pythonRLSA import rlsa
import math
x, y = mask.shape
value = max(math.ceil(x/100),math.ceil(y/100))+22#heuristic
mask = rlsa.rlsa(mask, True, False, value) #rlsa application
cv2.imshow('rlsah', mask)
cv2.imwrite('rlsah.png', mask)

contours,hierarchy = cv2.findContours(~mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) # find contours
mask2 = np.ones(image.shape, dtype="uint8") * 255 # blank 3 layer image
for contour in contours:
    [x, y, w, h] = cv2.boundingRect(contour)
    if w > 0.15*image.shape[1]: # width heuristic applied
        title = image[y: y+h, x: x+w] 
        mask2[y: y+h, x: x+w] = title # copied title contour onto the blank image
        image[y: y+h, x: x+w] = 255 # nullified the title contour on original image
        #title for img 002 and #content for 001,003 and 004
cv2.imshow('title', mask2)
cv2.imwrite('title.png', mask2)
cv2.imshow('content', image)
cv2.imwrite('content.png', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
# mask — ndarray we got after applying rlsah
# mask2 — blank array
#For img 003, 0.1, 4, 0.15(Dont Consider)
#For img 002 ,1,22,0.15## Standard
